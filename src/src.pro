#-------------------------------------------------
#
# Project created by QtCreator 2015-02-24T09:18:15
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = helloworld
CONFIG   += console warn_on
CONFIG   -= app_bundle

TEMPLATE = app

DESTDIR = ../bin
MOC_DIR = ../build/moc
RCC_DIR = ../build/rcc
UI_DIR = ../build/ui
unix:OBJECTS_DIR = ../build/o/unix
win32:OBJECTS_DIR = ../build/o/win32
win64:OBJECTS_DIR = ../build/o/win64
macx:OBJECTS_DIR = ../build/o/mac


SOURCES += main.cpp
